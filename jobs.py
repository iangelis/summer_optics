#!/usr/bin/env python3
from cpymad.madx import Madx

import os

import numpy as np


def generate_optics_by_step(betasteps, initial_optics_file, energy=7000):
    current_dir = os.getcwd()
    mad_dir = f"{current_dir}/.."

    betasteps = betasteps[1:]

    inames = [None] * (betasteps.size + 1)
    inames[0] = "start_of_collapse/{ns}".format(ns=initial_optics_file)
    for i, beta_step in enumerate(betasteps):
        outname = f"opt_collapse_{int(beta_step*1000)}_1500_step.madx"
        mad = Madx()
        mad.chdir(mad_dir)

        mad.input(
            """
        option,-echo,-info;
        system,"mkdir temp";
        call,file="acc-models-lhc/lhc.seq";
        call,file="acc-models-lhc/hllhc_sequence.madx";
        call,file="acc-models-lhc/toolkit/macro.madx";
        """
        )

        mad.input(f"""exec,mk_beam({energy});""")

        mad.input('call,file="{ns}";'.format(ns=inames[i]))

        mad.globals.betx_IP1 = beta_step
        mad.globals.betx0_IP1 = beta_step
        mad.globals.betx_IP5 = beta_step
        mad.globals.betx0_IP5 = beta_step

        mad.globals.bety_IP1 = beta_step
        mad.globals.bety0_IP1 = beta_step
        mad.globals.bety_IP5 = beta_step
        mad.globals.bety0_IP5 = beta_step

        mad.input(
            f"""
        call,file="acc-models-lhc/toolkit/rematch_hllhc.madx";
        exec, save_optics_hllhc("summer_studies/{outname}");
        """
        )

        # inames[i + 1] = f"start_of_collapse/{outname}"
        inames[i + 1] = f"summer_studies/{outname}"
        # break


# bsteps = np.arange(0.98, 1.100, 0.05)
bsteps = np.linspace(0.98, 1.100, 11)
print(bsteps)
generate_optics_by_step(bsteps, "opt_collapse_980_1500.madx", energy=7000)
